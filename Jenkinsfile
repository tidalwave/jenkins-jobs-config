pipeline
  {
    agent any
    stages {
        stage('Build') {
            steps {
                withMaven(jdk: 'AdoptiumJDK_21',
                          maven: 'Maven 3.9.9',
                          mavenSettingsConfig: '43178a6a-84bc-4e77-8996-a65ce31525f5',
                          mavenLocalRepo: '$JENKINS_HOME/maven-repositories/executors/$EXECUTOR_NUMBER',
                          traceability: false) {
                    sh 'mvn -B clean package -DskipTests -Pgenerate-revision-id'
                }
                script {
                    currentBuild.description = readFile('.description').replace('DESCRIPTION: ', '')
                }
            }
        }
        stage('Test') {
            steps {
                withMaven(jdk: 'AdoptiumJDK_21',
                          maven: 'Maven 3.9.9',
                          mavenSettingsConfig: '43178a6a-84bc-4e77-8996-a65ce31525f5',
                          mavenLocalRepo: '$JENKINS_HOME/maven-repositories/executors/$EXECUTOR_NUMBER',
                          traceability: false) {
                    sh 'mvn -B package -Pit.tidalwave-ci-v1,it.tidalwave-metrics-v2,it.tidalwave-monocle-profile-v1'
                }
            }
            post {
                always {
                    testNG()
                    chuckNorris()
                    jacoco()
                    recordCoverage(tools: [[pattern: '**/**.exec']])
                    recordIssues sourceCodeRetention: 'LAST_BUILD', tools: [
                        java(),
                        ajc(),
                        javaDoc(),
                        spotBugs(useRankAsPriority: true),
                        cpd(),
                        pmdParser(),
                        taskScanner(highTags: 'FIXME',
                                    normalTags: 'TODO',
                                    ignoreCase: true,
                                    includePattern: '**/*.java,**/*.xml,**/*.properties,**/*.md.vm',
                                    excludePattern: '**/target/**/*')
                    ]
                }
            }
        }
    }
}
